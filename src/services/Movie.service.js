import gql from 'graphql-tag';

const FETCH_MOVIE_BY_ID = gql`
  query movies {
    movie @rest(type: "Movies", path: "films/3/") {
      title
      director
      episode_id
      producer
    }
  }
`;

const FETCH_ALL_MOVIES = gql`
  query movies {
    allMovies @rest(type: "Movies", path: "films") {
      count
      next
      results
    }
  }
`;

const query = {
  fetchMovies: FETCH_ALL_MOVIES,
};

export default query;
