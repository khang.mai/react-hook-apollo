import React from 'react';
import {Text, View, Image} from 'react-native';

import assets from '../../assets';
import styles from './Movie.styles';

const Movie = ({item}) => {
  return (
    <View style={styles.container}>
      <View style={styles.avatarWrapper}>
        <Image
          resizeMode="contain"
          source={assets.MovieImage}
          style={styles.avatar}
        />
      </View>
      <View style={styles.content}>
        <Text style={styles.title}>{item.title}</Text>
        <Text style={styles.description}>Producer: {item.producer}</Text>
        <Text style={styles.price}>Release Date: {item.release_date}</Text>
      </View>
    </View>
  );
};

export default Movie;
