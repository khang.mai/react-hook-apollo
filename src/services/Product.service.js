import gql from 'graphql-tag';

const FETCH_ALL_PRODUCT = gql`
  query {
    all_product(locale: "en-us") {
      items {
        title
        description
        price
      }
    }
  }
`;

const query = {
  fetchProduct: FETCH_ALL_PRODUCT,
};
export default query;
