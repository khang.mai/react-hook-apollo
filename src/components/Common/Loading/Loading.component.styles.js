import {StyleSheet} from 'react-native';

export default StyleSheet.create({
  container: {
    paddingTop: 300,
    alignItems: 'center',
    justifyContent: 'center',
  },
});
