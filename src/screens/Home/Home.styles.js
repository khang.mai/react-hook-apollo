import {StyleSheet, Dimensions} from 'react-native';

const {width, height} = Dimensions.get('window');

const styles = StyleSheet.create({
  container: {
    paddingVertical: 10,
    width,
    height,
    justifyContent: 'center',
    alignItems: 'center',
  },
  contentContainerStyle: {
    paddingBottom: 35,
  },
  buttonStyle: {
    width: 80,
    height: 40,
    backgroundColor: 'blue',
    alignItems: 'center',
    justifyContent: 'center',
    marginVertical: 10,
  },
  buttonTitle: {
    color: 'white',
  },
});

export default styles;
