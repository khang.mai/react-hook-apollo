import React, {useState} from 'react';

import {View, FlatList} from 'react-native';
import MovieItem from '../../components/Movie/Movie.component';
import styles from './Movie.styles';
import clientRest from '../../services/Apollo.rest';
import MovieQuery from '../../services/Movie.service';
import BackButton from '../../components/Common/BackButton/BackButton.component';
import Loading from '../../components/Common/Loading/Loading.component';

const ClientRest = clientRest();

const renderItem = ({item}) => {
  return <MovieItem item={item} />;
};

const MovieScreen = ({onGoBack}) => {
  const [errors, setErrors] = useState(null);
  const [movies, setMovies] = useState([]);

  ClientRest.query({query: MovieQuery.fetchMovies})
    .then(response => {
      const {allMovies} = response.data;
      setMovies(allMovies.results);
    })
    .catch(err => {
      setErrors(err);
    });

  if (movies.length === 0 && !errors) return <Loading />;

  return (
    <View style={styles.container}>
      <BackButton onPress={onGoBack} />
      <FlatList
        data={movies}
        keyExtractor={(item, index) => item.title + index}
        renderItem={renderItem}
        contentContainerStyle={styles.contentContainerStyle}
      />
    </View>
  );
};

export default MovieScreen;
