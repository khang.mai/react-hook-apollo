import React from 'react';
import {ApolloProvider} from 'react-apollo';

import HomeScreen from './src/screens/Home/Home.screen';
import ProductScreen from './src/screens/Product/Product.screen';
import apolloClient from './src/services/Apollo';

const client = apolloClient();

const App = () => (
  <ApolloProvider client={client}>
    <HomeScreen />
  </ApolloProvider>
);

export default App;
