import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  clickable: {
    width: 80,
    height: 30,
    backgroundColor: 'grey',
    marginTop: 30,
    alignItems: 'center',
    justifyContent: 'center',
  },
  title: {
    fontWeight: 'bold',
    color: 'white',
  },
});

export default styles;
