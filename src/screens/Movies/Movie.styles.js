import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  container: {
    paddingVertical: 10,
  },
  contentContainerStyle: {
    paddingBottom: 35,
  },
  loading: {
    paddingTop: 300,
    alignItems: 'center',
    justifyContent: 'center',
  },
});

export default styles;
