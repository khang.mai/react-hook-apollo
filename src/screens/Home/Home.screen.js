import React, {useState} from 'react';

import {View, Text, TouchableOpacity} from 'react-native';
import styles from './Home.styles';
import ProductScreen from '../Product/Product.screen';
import MovieScreen from '../Movies/Movie.screen';

const HomeScreen = () => {
  const [activeScreen, setActiveScreen] = useState({});
  const onGoBack = () => setActiveScreen('Default');

  if (activeScreen === 'Products') return <ProductScreen onGoBack={onGoBack} />;
  if (activeScreen === 'Movies') return <MovieScreen onGoBack={onGoBack} />;

  return (
    <View style={styles.container}>
      <TouchableOpacity
        onPress={() => setActiveScreen('Products')}
        style={styles.buttonStyle}>
        <Text style={styles.buttonTitle}>Products</Text>
      </TouchableOpacity>
      <TouchableOpacity
        onPress={() => setActiveScreen('Movies')}
        style={styles.buttonStyle}>
        <Text style={styles.buttonTitle}>Movies</Text>
      </TouchableOpacity>
    </View>
  );
};

export default HomeScreen;
