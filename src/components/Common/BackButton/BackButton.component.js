import React from 'react';
import {Text, TouchableOpacity} from 'react-native';
import styles from './BackButton.component.styles';

const BackButton = ({onPress}) => {
  return (
    <TouchableOpacity onPress={onPress} style={styles.clickable}>
      <Text style={styles.title}>Back</Text>
    </TouchableOpacity>
  );
};

export default BackButton;
