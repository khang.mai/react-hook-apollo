import {ApolloClient} from 'apollo-client';
import {InMemoryCache} from 'apollo-cache-inmemory';
import {RestLink} from 'apollo-link-rest';
// Other necessary imports...

// Create a RestLink for the REST API
// If you are using multiple link types, restLink should go before httpLink,
// as httpLink will swallow any calls that should be routed through rest!
const restLink = new RestLink({
  uri: 'https://swapi.co/api/',
});

const clientRest = () => {
  return new ApolloClient({
    link: restLink,
    cache: new InMemoryCache(),
  });
};

export default clientRest;
