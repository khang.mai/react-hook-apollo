import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    padding: 5,
  },
  avatar: {
    width: 100,
    height: 100,
    backgroundColor: 'orange',
    marginRight: 10,
  },
  avatarWrapper: {
    borderColor: 'black',
    borderWidth: 1,
  },
  title: {
    fontWeight: 'bold',
    fontSize: 15,
    flexWrap: 'wrap',
    width: 250,
  },
  description: {
    fontWeight: '200',
    fontSize: 9,
    flexWrap: 'wrap',
    width: 250,
  },
  price: {
    fontSize: 12,
  },
  content: {
    paddingHorizontal: 5,
  },
});

export default styles;
