import React from 'react';
import {Text, View, Image} from 'react-native';

import styles from './Product.styles';
import assets from '../../assets';

const Product = ({item}) => {
  return (
    <View style={styles.container}>
      <View style={styles.avatarWrapper}>
        <Image
          resizeMode="contain"
          source={assets.KindleImage}
          style={styles.avatar}
        />
      </View>
      <View style={styles.content}>
        <Text style={styles.title}>{item.title}</Text>
        <Text style={styles.description}>{item.description}</Text>
        <Text style={styles.price}>{item.price} $$$</Text>
      </View>
    </View>
  );
};

export default Product;
