import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  container: {
    paddingVertical: 10,
  },
  contentContainerStyle: {
    paddingBottom: 35,
  },
});

export default styles;
