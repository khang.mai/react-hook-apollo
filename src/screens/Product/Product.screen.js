import React from 'react';

import {View, FlatList} from 'react-native';
import {useQuery} from 'react-apollo';
import Query from '../../services/Product.service';
import ProductItem from '../../components/Product/Product.component';
import styles from './Product.styles';
import BackButton from '../../components/Common/BackButton/BackButton.component';
import Loading from '../../components/Common/Loading/Loading.component';

const renderItem = ({item}) => {
  return <ProductItem item={item} />;
};

const ProductScreen = ({onGoBack}) => {
  const {loading, error, data} = useQuery(Query.fetchProduct);
  if (loading && !error) return <Loading />;

  const {
    all_product: {items: products},
  } = data;
  return (
    <View style={styles.container}>
      <BackButton onPress={onGoBack} />
      <FlatList
        data={products}
        keyExtractor={(item, index) => item.title + index}
        renderItem={renderItem}
        contentContainerStyle={styles.contentContainerStyle}
      />
    </View>
  );
};

export default ProductScreen;
