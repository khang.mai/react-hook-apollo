export default {
  MovieImage: require('./film-avatar.png'),
  KindleImage: require('./kindle-avatar.jpg'),
};
